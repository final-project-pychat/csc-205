1: INTRODUCTION
Altair 8800: "Economic" computer, made to be available on the consumer market. Availble in a kit and assembled (prebuilt) form.
Built on a Intel "Model 8080 Microcomputer", or what would now be known as a microprocessor. It also notes the usage of
LSI circuit and metal-oxide semiconductors, but it is precisely the scale ("a single silicon chip") that is the major technological breakthrough.

The manual and the design of the computer seem to be made so it can be used by a wide range of people, even those who do not know that much about
computers or electronics - thought it seems to be directed mainly at people who are familiar with at least one of the subjects.

Part 2 and 3: Organization and Operation of the Altair 8800. Low-level, the computer architecture.

A. LOGIC:
-Electronic logic revolves around boolean logic, with the on and off switches to make the statements:
AND OR NOT
"on" seems to be represents by 1, awhile "off" is represented by 0. The organization of on and off switches to form
different logic conditions are represnted in "truth tables", where on one side the condition is stated and the output is stated on the other side.


B. electronic logic
an electronic logic system is defined by the use of three or more logic circuits. For example, the exclusive or circuit uses 4 conditions.
First, it implements two boolean inputs (A and B) into two statements. An Or and AND statement. The Or statement leads to an AND statement, which if
satisfied, outputs the "sum" ("binary adder"). This AND statement's other input, however, is from a NOT device leading from the AND device, which means that if they
are both the same, then it will output the "carry." This circuit is now known as a XOR statement.

C.
-Most of the operations are performed in binary (0-1, base 2 number system). To convert binary to decimal, take teach "1". It represents 2 to the power of it's
place in the series of numbers. Add all of the values together and you have the full value. (For example, 1011 is 1 (2^0) + 2 (2^1) + 8 (2^3), which makes 11.)
Binary is read from left to right to determinet the number.

To convert binary into octal, they are sorted into three groups of bits. This consists of the first group, which is two bits, and then two groups of 3.
These groups are coded into binary and are then put together like addign a string to create the octal number.
For example, 10 + 010 + 011 would make 2 2 3, or 223 in octal.
